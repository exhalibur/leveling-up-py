import json
import os
from abc import ABC

import falcon
from app_code.learned_tasks.user_settings import USER
from app_code.learned_tasks.tasks import TASKS
from app_code.modules.errors import ERR_LIST
from config import compilefile, collectorfile


class Collector(ABC):
    main_route = collectorfile

    # TODO: Чекай главный путь!!!
    @classmethod
    def on_get(cls, req, resp, action):
        if action == 'get_values':
            get_settings = USER
            resp.body = json.dumps(get_settings)
            resp.status = falcon.HTTP_200
        if action == 'delete_values':
            USER['coins'] = 0
            for level in TASKS.keys():
                for task in TASKS[level]:
                    task['is_task_completed'] = 0
            resp.status = falcon.HTTP_200

    @classmethod
    def on_post(cls, req, resp, action):
        hard_level = int(req.media.get('level'))
        if str(hard_level) not in TASKS.keys():
            raise falcon.HTTPStatus(
                falcon.HTTP_400,
                body=json.dumps({'success': False,
                                 'description': 'Такого блока не существует!'}),
            )
        if action in ('check_task', 'get_task',):
            task_number = int(req.media.get('task'))
            task_info = TASKS[str(hard_level)][task_number %
                                               len(TASKS[str(hard_level)])]
            check_easy_string = """\n"""+task_info.get('test_string')+"""\nwith open('""" + \
                cls.main_route+""".txt', 'w') as f:\n\tf.write(str(ans))"""

            # Если человек на "приветственном" слове
            if task_number == -1:
                raise falcon.HTTPStatus(
                    falcon.HTTP_200,
                    body=json.dumps({'success': False,
                                     'description': 'Быстрее переходите к заданию, время не ждет'}
                                    )
                )

        if action == 'check_task':
            test_string = req.media.get('task_string')
            with open('output.py', 'w') as f:
                f.write(test_string + check_easy_string)
            # Компилируем на проверку
            response = os.system(compilefile)
            # Ошибка компиляции
            if response != 0:
                resp.body = json.dumps({'success': False,
                                        'description': 'Не компилируемый фаил!',
                                        'result': test_string})
                raise falcon.HTTPStatus(falcon.HTTP_400,
                                        body=json.dumps(
                                            {'success': False,
                                             'description': ERR_LIST['wrong']}
                                        ))
            # Проверяем ответ на правильность
            response = ''
            with open(str(cls.main_route+'.txt'), 'r') as f:
                for line in f:
                    response += line
            if response == task_info.get('correct_answer') and task_info.get('is_task_completed') == 0:
                resp.body = json.dumps({'success': True,
                                        'description': 'Правильно!',
                                        'hard_level': task_info.get('hard_level') + 1
                                        })
                task_info['is_task_completed'] = 1
                USER['coins'] += task_info.get('hard_level') + 1
            else:
                if task_info.get('is_task_completed') == 1:
                    resp.body = json.dumps({'success': False,
                                            'description': 'Вы уже решали эту задачу, не жадничай.',
                                            })
                else:
                    raise falcon.HTTPStatus(
                        falcon.HTTP_200,
                        body=json.dumps({'success': False,
                                         'description': 'Не правильно! Попробуйте еще.'}
                                        )
                    )
            resp.status = falcon.HTTP_200

        if action == 'get_task':
            result_info = task_info
            resp.status = falcon.HTTP_200
            resp.body = json.dumps({'success': True,
                                    'description': '',
                                    'result': result_info})

        if action == 'change_lvl':
            USER['level'] = hard_level
            resp.status = falcon.HTTP_200
            resp.body = json.dumps({'success': True,
                                    'description': ''})
