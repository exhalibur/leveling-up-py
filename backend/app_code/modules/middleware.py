"""Middleware module."""

from datetime import datetime

import falcon
from app_code.modules.errors import ERR_LIST

from config import LOGFILE
from falcon.http_status import HTTPStatus
from loguru import logger

logger.add(LOGFILE, rotation="50 MB")


class LogWrite():
    """ логирование результатов отработки API """

    def process_request(self, req, _):
        """Записываем время начала отработки.

        Args:
            req: Данные поступившего запроса.

        """

        req.context["start_time"] = datetime.now()

    @classmethod
    def process_response(cls, req, resp, resource, req_succeeded):
        # pylint: disable=unused-argument
        """Записываем в лог файл.

        Args:
            req: Данные поступившего запроса.
            resp: Данные для отправки ответа.
            resource: Ресурсы поступившего запроса.
            req_succeeded: Стандарт falcon.

        """
        now = datetime.now()
        delta = now - req.context["start_time"]
        time = int(delta.total_seconds() * 1000)
        if resp.status[:3] == '200':

            logger.info("{} {} | {} | {} ms | {}".format(
                req.method,
                req.path,
                resp.status,
                time,
                req.content_length
            )
            )
        else:
            error_text = "{} {} | {} | {} ms \n {}".format(
                req.method,
                req.path,
                resp.status,
                time,
                req.media
            )
            logger.error(error_text)

class RequireJSON(object):
    """Check valid JSON."""

    @classmethod
    def process_request(cls, req, _):
        """Check header.

        Args:
            req: Данные поступившего запроса.
            _ : ignored params

        Raises:
            falcon.HTTPNotAcceptable: Тип ожидаемого ответа не поддерживается.
            falcon.HTTPUnsupportedMediaType: Тип файла не поддерживается.
        """
        if not req.client_accepts_json:
            raise falcon.HTTPNotAcceptable(ERR_LIST['no_JSON'])
        if req.method in ('POST', 'GET',):
            if req.content_type:
                if 'application/json' not in req.content_type:
                    raise falcon.HTTPUnsupportedMediaType(ERR_LIST['no_JSON'])


class HandleCORS(object):
    """Set CORS headers."""

    def process_request(self, req, resp):
        """Set header.

        Args:
            req ([type]): Данные поступившего запроса.
            resp ([type]): Данные для отправки ответа.
        """
        resp.set_header('Access-Control-Allow-Origin', '*')
        resp.set_header("Access-Control-Allow-Methods", "*")
        resp.set_header('Access-Control-Allow-Headers', "*")
        resp.set_header('Access-Control-Max-Age', 1728000)  # 20 days
        if req.method == 'OPTIONS':
            raise HTTPStatus(falcon.HTTP_200, body='\n')
