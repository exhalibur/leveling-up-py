"""Инициализации приложения."""
from datetime import datetime
import os
import falcon
from loguru import logger

from app_code.modules.middleware import HandleCORS, LogWrite, RequireJSON
from config import LOGFILE
from app_code.exhample_parse import Collector

logger.add(LOGFILE, rotation="50 MB")
APP = falcon.API(
    middleware=[
        LogWrite(),
        HandleCORS(),
        RequireJSON(),
    ],
)
answer_giver = Collector()
APP.add_route('/api/{action}', answer_giver)
