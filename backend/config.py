"""Конфигурация программного обеспечения."""
import configparser

CONFIG_FILE = 'config.ini'
config = configparser.ConfigParser()
config.read(CONFIG_FILE)

LOGFILE = config.get('DEFAULT', 'LOGFILE')

compilefile = config.get('DATASET', 'compilefile')
collectorfile = config.get('DATASET', 'collectorfile')